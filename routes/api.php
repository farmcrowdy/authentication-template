<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'API\RegisterController@authenticate');
Route::post('login', 'API\RegisterController@authenticate');
Route::post('otp/resend', 'API\RegisterController@authenticate');

Route::group(['prefix'=> 'password'], function (){
    Route::post('/request', 'API\RegisterController@requestPasswordChange');//request for a password change
    Route::post('/update', 'API\RegisterController@updatePassword');//Update default user password.
    Route::post('/change', 'API\RegisterController@changePassword');//take this down soon
});

Route::middleware('auth:api')->group(function () {
    Route::resource('products', 'API\ProductController');
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
});
