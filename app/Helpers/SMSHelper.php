<?php
namespace App\Helpers;

use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class SMS {

    /**
     * @param int $to
     * @param int $code
     * @return bool|string
     */
    public static function sendOTP(int $to, int $code)
    {
        $client = (new SMS)->initialize();
        try {
            $client->messages->create(
                $to,
                [
                    'from' => getenv('APP_NAME'),
                    'body' => "Your OTP is $code",
                ]
            );

        } catch (TwilioException $e) {
            return $e->getMessage();
        }
        return true;
    }

    /**
     * @return string|Client
     */
    private function initialize(){

        $sid    = getenv('TWILIO_SID');
        $token  = getenv('TWILIO_TOKEN');

        try {
            $client = new Client($sid, $token);
        } catch (ConfigurationException $e) {
            return  $e->getMessage();
        }

        return $client;
    }
}
