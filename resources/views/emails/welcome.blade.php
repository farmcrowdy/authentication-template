<!DOCTYPE html>
<html>

    <head>
        <title>{{ getenv('APP_NAME') }} | Email Verified</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://plentywaka.com/avenir-font/stylesheet.css">
        <style>
            h2{line-height: 30px!important;}
        </style>
    </head>

    <body
        style="
            width: 100% !important;
            font-size: 16px;
            -webkit-text-size-adjust: 100%;
            overflow-wrap:break-word;
            word-wrap:break-word;
            hyphens:auto;
            background: #efefef !important;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
            font-family: 'Avenir LT Std',sans-serif;
            line-height: 160%;
            color: #222;">

        <table width="500px" cellspacing="15px" cellpadding="0"  style="margin:0 auto;">
            <tbody>

                <tr>
                    <td style="height: 10px; ">
                        <div></div>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:fefefe;">
                        <div style="display: block;	text-align:center; margin: 0px;  padding:70px 0; padding-bottom:10px;">
                            <img style="width:154px; height:auto;" src="{{asset('images/pw_logo.png')}}" alt="{{ getenv('APP_NAME') }} Logo">

                        </div>

                    </td>
                </tr>

                <tr>
                    <td style="background-color:#fff; font-family: 'Avenir LT Std', sans-serif; ">
                        <div style="display: block; margin: 0px; padding:40px; padding-top: 20px; padding-bottom: 10px; background-color:#fff; font-family: 'Avenir LT Std', sans-serif; ">

                            <div style="padding-bottom: 30px;">
                                <h2 style="color: #121212; margin-top:15px; font-weight: bold; font-size: 1.3rem;text-align: center;">Welcome!</h2>
                                <p style=" color: #121212; margin-bottom: 0.2rem; line-height: 1.4; font-weight: 300;"> Thank you for signing up with {{ getenv('APP_NAME') }}! We hope you enjoy your time with us.</p>
                            </div>

                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center; background-color:#fff; color:#222; font-family: 'Avenir LT Std', sans-serif; ">

                        <div style="width:80%; margin: 0 auto; background: #fff;">
                            <div style="padding:40px;">
                                <h3 style="margin-bottom:20px; margin: 0; font-weight: 500;">Need help?</h3>
                                <p style="margin-bottom: 0; color: #3a3636;">Please send any feedback or report to</p>
                                <a style="color: #121212; text-decoration: none; max-width: 75%; margin: 0 auto; line-height: 1.5; display: block;" href="#">{{ getenv('APP_EMAIL') }}</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 60px;">
                        <div></div>
                    </td>
                </tr>
            </tbody>
        </table>


    </body>

</html>
