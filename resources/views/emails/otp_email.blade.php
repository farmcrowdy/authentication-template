<!DOCTYPE html>
<html>

<head>
    <title>{{ getenv('APP_NAME') }} | OTP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://plentywaka.com/avenir-font/stylesheet.css">


</head>

<body style="width: 100% !important; -webkit-text-size-adjust: 100%; overflow-wrap:break-word; word-wrap:break-word;hyphens:auto; background: #efefef !important; -ms-text-size-adjust: 100%; margin: 0;
padding: 0; font-family: 'Avenir LT Std',sans-serif; line-height: 160%; font-size: 16px;color: #222;">

<table width="500px" cellspacing="0" cellpadding="0"  style="margin:0 auto;">
    <tbody>

    <tr>
        <td style="height: 60px; ">
            <div></div>
        </td>
    </tr>
    <tr>
        <td style="background-color:#ffffff;">
            <div style="display: block;	text-align:center; margin: 0px;  padding:20px; padding-top:70px;">
                <img style="width:117px; height:auto;" src="{{ asset('images/pw_logo.png') }}" alt="{{ getenv('APP_NAME') }} Logo">

            </div>
            <div style="-webkit-border:1px solid rgba(112, 112, 112, 22%);
                    -moz-border:1px solid rgba(112, 112, 112, 22%);
                    -o-border:1px solid rgba(112, 112, 112, 22%);
                    -ms-border:1px solid rgba(112, 112, 112, 22%);
                    border:1px solid rgba(112, 112, 112, 22%); width:80%; margin:0 auto;"></div>

        </td>
    </tr>

    <tr>
        <td style="background-color:#fff; font-family: 'Avenir LT Std', sans-serif; ">
            <div style="display: block; margin: 0px; padding-top: 20px; background-color:#fff; font-family: 'Avenir LT Std', sans-serif; ">
                <div style="text-align: center;">
                    <h2 style="color: #121212; font-weight: 600; font-size: 1.5rem;">Hi, {{$recipient_name}}</h2>
                    <p style="max-width: 75%; line-height: 1.5; font-weight: 300; margin: 0 auto; color: #121212;">
                        We received a request to update your {{ getenv('APP_NAME') }} app details. We're here to help you!
                    </p>
                    <p>
                        Simply copy and paste the below code to your app to continue.
                    </p>
                    <div style="padding: 30px;">
                        <h2 style="color: #121212; font-weight: 600; font-size: 2rem;">{{$otp}}</h2>
                    </div>

                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding:10px 0; text-align:center; background-color:#fff; color:#222; font-family: 'Avenir LT Std', sans-serif; ">

            <div style="width:80%; margin: 0 auto; background: #F5F5F5;">
                <div style="padding:40px;">
                    <h3 style="margin-bottom:20px; margin: 0;">Need help?</h3>
                    <p style="margin-bottom: 0; color: #3a3636;">Please send any feedback or report to</p>
                    <a style="text-decoration: none; color: #121212; max-width: 75%; margin: 0 auto; line-height: 1.5; display: block;" href="#">{{ getenv('APP_EMAIL') }}</a>
                </div>
            </div>
            <div style=" padding-top:80px;"></div>
        </td>
    </tr>
    <tr>
        <td style="height: 60px;">
            <div></div>
        </td>
    </tr>
    </tbody>
</table>


</body>

</html>
